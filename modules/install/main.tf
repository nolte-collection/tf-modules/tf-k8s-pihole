
variable "namespace" {
  default = "pihole"
}


resource "kubernetes_namespace" "rook" {
  metadata {
    name = var.namespace
  }
}


variable "image_tag" {
  default = "v5.1.2"
}
variable "lp_ip" {

}

variable "extra_dns_entries" {
  type    = list(string)
  default = []
}

locals {
  DEFAULT_SERVICE = {
    loadBalancerIP = var.lp_ip
    "annotations" = {
      "metallb.universe.tf/allow-shared-ip" : "generic-cluster-pool"
    }
    type = "LoadBalancer"
  }
}

locals {
  EXTRA_VALUES = {
    extraEnvVars = {
      "ServerIP" = var.lp_ip
    }
    serviceTCP = local.DEFAULT_SERVICE
    serviceUDP = local.DEFAULT_SERVICE
    dnsmasq = {
      customDnsEntries = var.extra_dns_entries
    }
  }

}

# https://github.com/MoJo2600/pihole-kubernetes/blob/master/charts/pihole/values.yaml
resource "helm_release" "release" {
  name       = "pihole"
  repository = "https://mojo2600.github.io/pihole-kubernetes/"
  chart      = "pihole"
  namespace  = kubernetes_namespace.rook.metadata[0].name
  values = [
    file("${path.module}/files/values.yaml"),
    #"${file("${path.module}/files/values.yaml")}"
    yamlencode(local.EXTRA_VALUES)
  ]
  set {
    name  = "image.tag"
    value = var.image_tag
  }
}
